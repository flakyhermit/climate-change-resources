# Climate Change Resources
A collection of resources on climate change. 

## Contents
- Scientific Papers
- [Books](## Books)
- Articles
- Web Resources

## Books
- [This Changes Everything: Capitalism vs. Climate](https://www.goodreads.com/book/show/21913812-this-changes-everything?ac=1&from_search=true&qid=5D4eoQ2hKu&rank=3) - *Naomi Klein*
- [The Uninhabitable Earth](https://www.goodreads.com/book/show/41552709-the-uninhabitable-earth) - *David Wallace-Wells*
- [The Great Derangement](https://www.goodreads.com/book/show/29362082-the-great-derangement) - *Amitav Ghosh*

## Web Resources
**Blogs**
- [NASA Climate Blog](https://climate.nasa.gov/blog/) - Climate change news and facts, straight from NASA.

**Organizations**
- [Climate Central](https://www.climatecentral.org/) - A prominent organization of scientists and journalists who researches on climate change to reports the finding to the general public.

**Social Media**
- [/r/climatechange](https://old.reddit.com/r/climatechange/) on Reddit

---
**Contributions are welcome!** If you find out that a vital resource is missing, feel free to add it. Issue a pull request. If you're not technically inclined drop me an email and I will add it to the list. 
